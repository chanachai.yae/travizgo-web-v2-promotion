function getLang(...args: ((prevResult: string) => (string | undefined | null))[]): string {
    if (!args) return ''
    const lang = args?.reduce((acc, arg) => {
        if (acc) return acc
        const result = arg(acc)
        if (result) {
            return result
        }
        return acc
    }, '')
    return lang
}

export default getLang