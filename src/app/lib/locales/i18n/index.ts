
import { createInstance, i18n } from 'i18next'
import ChainedBackend from 'i18next-chained-backend'
import { initReactI18next } from 'react-i18next/initReactI18next'

import { getOptions } from './settings'
import { defaultNS } from '@/config/default-variable'

export const i18nextInstanceServer = createInstance()
const initI18next = async (lng, ns): Promise<i18n> => {
  await i18nextInstanceServer
    .use(initReactI18next)
    .use(ChainedBackend)
    .init(getOptions(lng, ns))
  return i18nextInstanceServer
}

export async function useTranslation(lng: string, ns: string = defaultNS, options: any = {}) {
  const i18nextInstance = await initI18next(lng, ns)
  return {
    t: i18nextInstance.getFixedT(lng, Array.isArray(ns) ? ns[0] : ns, options.keyPrefix),
    i18n: i18nextInstance
  }
}