'use client'

import { lstatSync, readdirSync } from 'fs';
import i18next from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import I18NextChainedBackend from 'i18next-chained-backend'
import FSBackend from 'i18next-fs-backend/cjs';
import I18NextHttpBackend from 'i18next-http-backend/cjs';
import resourcesToBackend from 'i18next-resources-to-backend'
import { useTranslation as useTranslate } from 'next-i18next'
import { dirname, join } from 'path';
import {
  initReactI18next
} from 'react-i18next'
import { fileURLToPath } from 'url';

import { getOptions } from './settings'
import { BASE_SUBPATH } from '@/config/uri-setting';

type AddResourceType = {
  lng: string
  ns: string
  resources: any
  deep?: boolean
  overwrite?: boolean
}

type ChangeLanguage = {
  lang: string
}

const runsOnServerSide = typeof window === 'undefined'
const preloadFn = () => {
  if (!runsOnServerSide) return []
  const __dirname = dirname(fileURLToPath(import.meta.url))
  const localesFolder = join(__dirname, '../../../public/locales_cache')
  const preload = readdirSync(localesFolder).filter((fileName) => {
    const joinedPath = join(localesFolder, `${fileName}`)
    const lstat = lstatSync(joinedPath)
    const isDirectory = lstat.isDirectory()
    return isDirectory
  })

}
const loadPath = () => {
  if (!runsOnServerSide) return ''
  const __dirname = dirname(fileURLToPath(import.meta.url))
  const localesFolder = join(__dirname, '../../../public/locales_cache')
  const path = join(localesFolder, '{{lng}}/{{ns}}.json')
  return path

}
i18next
  .use(initReactI18next)
  .use(LanguageDetector)
  .use(FSBackend)
  .use(I18NextChainedBackend)

if (!i18next.isInitialized) {
  (async () => {
    await i18next
      .init({
        ...getOptions(),
        lng: undefined,
        detection: {
          order: ['cookie', 'path', 'htmlTag', 'navigator'],
          lookupCookie: 'lang'
        },

        // backend: {
        //   backends: [
        //     FSBackend,
        //     HttpBackend,
        //   ],
        //   backendOptions: [{
        //     loadPath: './public/locales_cache/{lng}/{ns}.json',
        //   }, {
        //     loadPath: 'hotel/locales_cache/{lng}/{ns}.json'
        //     // 'requestOptions': {
        //     //   'next': {
        //     //     'revalidate': 60 * 60
        //     //   }
        //     // }
        //   }]
        // },

        backend: {
          backends: runsOnServerSide ? [I18NextHttpBackend,
            resourcesToBackend((language, namespace, callback) => {
              import(`../../../public/locales_cache/${language}/${namespace}.json`)
                .then(({ default: resources }) => {
                  callback(null, resources)
                })
                .catch((error) => {
                  callback(null, {})
                })
            })
          ] : [
            I18NextHttpBackend,
            resourcesToBackend((language, namespace, callback) => {
              import(`../../../public/locales_cache/${language}/${namespace}.json`)
                .then(({ default: resources }) => {
                  callback(null, resources)
                })
                .catch((error) => {
                  callback(null, {})
                })
            })
          ],
          backendOptions: [{
            loadPath: `${BASE_SUBPATH}/locales_cache/{lng}/{ns}.json`,
          }]

        }

      })
  }
  )()
}

export function useTranslation(lng?: string) {
  const ret = useTranslate('translation')
  const { i18n, t } = ret
  // if (runsOnServerSide && typeof i18nextInstanceServer !== 'undefined') {
  //   return {
  //     t: i18nextInstanceServer.getFixedT(lng as any, 'translation', ''),
  //     i18n: i18nextInstanceServer
  //   }
  // }
  if (lng && runsOnServerSide && i18n.resolvedLanguage !== lng) {
    i18n.changeLanguage(lng)
  }

  return { t, i18n }
}

const i18n = {
  /**
   * @returns {Promise}
   */

  addResource: (param: AddResourceType) => {
    return i18next.addResourceBundle(param.lng, param.ns, param.resources)
  },
  changeLanguage: (param: ChangeLanguage) => {
    return i18next.changeLanguage(param.lang.toLowerCase())
  },
  getDataByLanguage: (lang: string) => {
    return i18next.getDataByLanguage(lang)
  },
  t: (key: string, option?: any): string => {
    return i18next.t(key, option) as string
  },
  exists: (key: string, option?: Object) => {
    return i18next.exists(key, option)
  },
}

export default i18n
