import { InitOptions } from 'i18next';
import FSBackend, { FsBackendOptions } from 'i18next-fs-backend/cjs';
import HttpBackend, { HttpBackendOptions } from 'i18next-http-backend/cjs';

import LabelManager from "@/common/manager/LabelManager";
import { defaultNS, defaultLanguage, languages } from '@/config/default-variable';

export function getOptions(lng = defaultLanguage, ns = defaultNS): InitOptions & {
  backend: { backends: any[], backendOptions: [FsBackendOptions & { expirationTime?: number }, HttpBackendOptions] }
} {
  return {
    supportedLngs: languages,
    // fallbackLng,
    lng,
    fallbackNS: defaultNS,
    defaultNS,
    ns,
    backend: {
      backends: [
        FSBackend,
        HttpBackend,
      ],
      backendOptions: [{
        loadPath: './public/locales_cache/{lng}}/{ns}.json',
        addPath: './public/locales_cache/{lng}/{ns}.json',
        expirationTime: 60 * 60 * 1000
      }, {
        loadPath: process.env.LOCALES_ENDPOINT,
        parse: function (data: any) {
          return LabelManager.default.convertResource(data)
        }
        // 'requestOptions': {
        //   'next': {
        //     'revalidate': 60 * 60
        //   }
        // }
      }]
    },
    interpolation: {
      escapeValue: false,
      prefix: '{',
      suffix: '}',
    },
    nsSeparator: ';;',
    keySeparator: ';;'
  }
}


