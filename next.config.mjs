import bundleAnalyzer from '@next/bundle-analyzer'

const withBundleAnalyzer = bundleAnalyzer({
  enabled: process.env.ANALYZE === 'true',
})
/** @type {import('next').NextConfig} */

const nextConfig = {
  output: 'standalone',
  reactStrictMode: false,
  compiler: {
    // removeConsole: process.env.NODE_ENV === 'production' ? { exclude: ['error', 'debug'] } : false,
    styledComponents: {
      displayName: process.env.NODE_ENV === 'production' ? false : true,
      ssr: true,
    },
  },
  logging: {
    fetches: { fullUrl: true },
  },
  experimental: {
    optimizeCss: true,
    appDir: true,
    esmExternals: true,
    forceSwcTransforms: true,
  },
  images: {
    domains: ['dev-assets.travizgo.dev', 'assets.travizgo.com'],
    deviceSizes: [280, 360, 412, 576, 640, 768, 828, 1024, 1080, 1200],
    formats: ['image/avif', 'image/webp'],
  },
  webpack: (config) => {
    config.resolve.fallback = { fs: false }

    return config
  },
  typescript: {
    ignoreBuildErrors: true,
  },
  trailingSlash: true,
  // modularizeImports: {
  //   lodash: {
  //     transform: 'lodash/{{member}}',
  //     preventFullImport: true,
  //   },
  // },
  basePath: Boolean(process.env.NEXT_PUBLIC_PREFIX_PATH) ? `/${process.env.NEXT_PUBLIC_PREFIX_PATH}` : undefined,
  assetPrefix: Boolean(process.env.NEXT_PUBLIC_PREFIX_PATH) ? `/${process.env.NEXT_PUBLIC_PREFIX_PATH}` : undefined,
  async redirects() {
    return [
      {
        source: '/retrieve',
        has: [
          {
            type: 'query',
            key: 'ref1',
            value: '(?<ref1>.*)',
          },
          {
            type: 'query',
            key: 'ref2',
            value: '(?<ref2>.*)',
          },
        ],
        destination: '/retrieve/?bookingId=:ref1&lastName=:ref2&ref1&ref2',
        permanent: true,
      },
    ]
  },
  async rewrites() {
    return [
      {
        destination: '/api/:path*',
        source: `/api/:path*`,
      },
    ]
  },
}

export default withBundleAnalyzer(nextConfig)
