import 'styled-components'

import { ThemeInterface } from '@ttcg-developers/travizgo-web-uikit-2/dist/commons/theme';

declare module 'styled-components' {
  export interface DefaultTheme extends ThemeInterface { }
}
